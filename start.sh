#!/bin/sh

# Script adapted from yellowiscool/emqttd-cluster

SELF_HOST=$(hostname)
SELF_IP=$(grep ${SELF_HOST} /etc/hosts | tail -n 1 | cut -f 1)

sed -i -e "s/^-name\s*.*@.*/-name emqttd@${SELF_IP}/g" $EMQTTD_HOME/etc/vm.args

emqttd start

# wait and ensure emqttd status is running
while [ x$(emqttd_ctl status |grep 'is running'|awk '{print $1}') = x ]
do
    sleep 1
    echo '['$(date -u +"%Y-%m-%dT%H:%M:%SZ")']:waiting emqttd'
done

echo '['$(date -u +"%Y-%m-%dT%H:%M:%SZ")']:emqttd start'

sleep 5
for MASTER in $(echo $MASTER_NODES); do
    echo '['$(date -u +"%Y-%m-%dT%H:%M:%SZ")']:join emqttd@'${MASTER}
    emqttd_ctl cluster join 'emqttd@'${MASTER}
done

# Logging
 tail -F $EMQTTD_HOME/log/*
