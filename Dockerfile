FROM henho/docker-erlang:latest
MAINTAINER An Phung <an.phungle@gmail.com>

# Adapted from devicexx/emqttd
ARG EMQTTD_VERSION

ENV EMQTTD_VERSION=${EMQTTD_VERSION:-master} \
    EMQTTD_HOME=${EMQTTD_HOME:-/opt/emqttd}

ENV PATH=$PATH:$EMQTTD_HOME/bin

# Download and build emqtt
RUN apk update \
    && apk --no-cache add \
        perl \
        git \
        make \
    && git clone -b ${EMQTTD_VERSION} https://github.com/emqtt/emqttd-relx.git /emqttd \
    && cd /emqttd \
    && make \
    && mkdir /opt && mv /emqttd/_rel/emqttd /opt/emqttd \
    && cd / && rm -rf /emqttd \
    && apk --purge del \
        git \
        make \
    && rm -rf /var/cache/apk/*

VOLUME ["$EMQTTD_HOME/etc", "$EMQTTD_HOME/log", "$EMQTTD_HOME/data"]

WORKDIR $EMQTTD_HOME

# Setup start script
COPY ./start.sh $EMQTTD_HOME/start.sh
RUN chmod +x $EMQTTD_HOME/start.sh
CMD ${EMQTTD_HOME}/start.sh

# emqttd will occupy 1883 port for MQTT, 8883 port for MQTT(SSL), 8083 for WebSocket/HTTP, 18083 for dashboard
EXPOSE 1883 8883 8083 18083
